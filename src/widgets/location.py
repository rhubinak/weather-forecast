"""
widgets.py - Widgets for the Weather Forecast application.

Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from java.io import BufferedReader, File, FileNotFoundException, FileReader, \
                    FileWriter
from java.lang import Double, NumberFormatException, Object, String
from java.util import Arrays, HashMap
from android.content import Context
from android.os import Environment
from android.view import Gravity, View, ViewGroup
from android.widget import AdapterView, AutoCompleteTextView, Button, \
    FrameLayout, ImageButton, LinearLayout, ListView, RelativeLayout, \
    TextView, ViewAnimator

import android.R

from serpentine.files import Files
from serpentine.adapters import FilterStringArrayAdapter, StringListAdapter
from serpentine.strings import Strings
from serpentine.widgets import HBox

from app_resources import R

from configuration import ConfigureListener, ConfigureWidget


class Coordinates(Object):

    __fields__ = {
        "latitude": str,
        "longitude": str,
        "altitude": str,
        "defined": bool
        }
    
    def __init__(self):
        Object.__init__(self)
        self.defined = False
    
    @args(void, [String, String, String])
    def __init__(self, latitude, longitude, altitude):
        Object.__init__(self)
        
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.defined = True
    
    @args(String, [])
    def toString(self):
        return self.latitude + " " + self.longitude + " " + self.altitude


class AddLocationListener:

    @args(void, [String, String])
    def addLocation(self, name, spec):
        pass


class LocationListener:

    @args(void, [String, Coordinates])
    def locationEntered(self, name, coordinates):
        pass


class RemoveLocationListener:

    def removeLocation(self):
        pass
    
    def cancelRemove(self):
        pass


class ModeHandler:

    def enterMenuMode(self):
        pass
    
    def enterAddMode(self):
        pass
    
    def enterRemoveMode(self):
        pass
    
    def startConfiguration(self):
        pass


class LocationAdapter(StringListAdapter):

    def __init__(self, strings):
    
        StringListAdapter.__init__(self, strings)
    
    def getView(self, position, convertView, parent):
    
        # If convertView is not None then reuse it.
        if convertView != None:
            return convertView
        
        view = TextView(parent.getContext())
        view.setText(self.items[position])
        view.setTextSize(view.getTextSize() * 1.25)
        return view


class LocationWidget(RelativeLayout):

    __interfaces__ = [AdapterView.OnItemClickListener,
                      AdapterView.OnItemLongClickListener,
                      AddLocationListener, RemoveLocationListener,
                      ModeHandler]
    
    __fields__ = {"mode": str}
    
    @args(void, [Context, LocationListener, ConfigureListener, HashMap(String, Coordinates)])
    def __init__(self, context, locationHandler, configureHandler, coordinates):
    
        RelativeLayout.__init__(self, context)
        
        self.configureHandler = configureHandler
        self.coordinates = coordinates
        self.currentItem = -1
        self.mode = "menu"
        self.previous_mode = "menu"
        
        self.locationHandler = locationHandler
        self.adapter = self.getAdapter()
        
        self.listView = ListView(context)
        self.listView.setAdapter(self.adapter)
        self.listView.setOnItemClickListener(self)
        self.listView.setOnItemLongClickListener(self)
        
        self.views = ViewAnimator(context)
        self.views.addView(MenuWidget(context, self), 0, self.getViewParams())
        self.views.addView(AddWidget(context, self, self), 1, self.getViewParams())
        self.views.addView(RemoveWidget(context, self), 2, self.getViewParams())
        self.views.setDisplayedChild(0)
        self.views.setId(1)
        
        self.view_indices = {"menu": 0, "add": 1, "remove": 2}
        
        listParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        listParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        listParams.addRule(RelativeLayout.ABOVE, 1)
        
        self.addView(self.listView, listParams)
        self.addView(self.views, self.getAddParams())
    
    def readLocations(self):
    
        self.locations = {}
        self.order = []
        
        if Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED:
            return
        
        storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS)
        
        subdir = File(storageDir, "WeatherForecast")
        if not subdir.exists():
            return
        
        f = File(subdir, "locations.txt")
        
        try:
            stream = BufferedReader(FileReader(f))
        except FileNotFoundException:
            return
        
        while True:
        
            line = stream.readLine()
            if line == None:
                break
            
            line = line.trim()
            
            # Distinguish between pure location entries and ones with
            # coordinates.
            if " " in line:
                # Format: name latitude longitude altitude
                pieces = Arrays.asList(line.split(" "))
                
                l = len(pieces)
                
                if l < 4:
                    continue
                
                latitude, longitude, altitude = pieces[l - 3:]
                try:
                    Double.parseDouble(latitude)
                    Double.parseDouble(longitude)
                    Double.parseDouble(altitude)
                except NumberFormatException:
                    continue
                
                place = Strings.join(" ", pieces[:l - 3])
                coords = Coordinates(latitude, longitude, altitude)
            else:
                # Slash-separated place specification
                pieces = Arrays.asList(line.split("/"))
                
                if len(pieces) < 3:
                    continue
                
                # Properly regenerate the name by converting underscores
                # to spaces and stripping any trailing text that starts
                # with a tilde.
                place = pieces[len(pieces) - 1].replace("_", " ")
                if "~" in place:
                    place = place[:place.indexOf("~")]
                
                try:
                    coords = self.coordinates[line.toLowerCase()]
                except KeyError:
                    continue
            
            self.locations[place] = coords
            self.order.add(place)
        
        stream.close()
    
    def writeLocations(self):
    
        f = Files.createExternalFile(Environment.DIRECTORY_DOWNLOADS,
            "WeatherForecast", "locations.txt", None, None)
        
        try:
            stream = FileWriter(f)
        except FileNotFoundException:
            return
        
        for key in self.order:
            stream.write(key + " " + str(self.locations[key]) + "\n")
        
        stream.flush()
        stream.close()
    
    @args(LocationAdapter, [])
    def getAdapter(self):
    
        self.readLocations()
        
        keys = []
        for location in self.order:
            keys.add(location)
        
        return LocationAdapter(keys)
    
    def onItemClick(self, parent, view, position, id):
    
        try:
            location = self.adapter.items[int(id)]
            self.locationHandler.locationEntered(location, self.locations[location])
        except IndexError:
            pass
        
        if self.currentItem != -1:
            self.enterAddMode()
    
    def addLocation(self, name, spec):
    
        if self.locations.containsKey(name):
            return
        
        try:
            coordinates = self.coordinates[spec.toLowerCase()]
        except KeyError:
            return
        
        self.locations[name] = coordinates
        self.order.add(name)
        
        self.adapter.items.add(name)
        self.listView.setAdapter(self.adapter)
        self.enterMenuMode()
    
    def onItemLongClick(self, parent, view, position, id):
    
        if self.mode != "remove":
            self.currentItem = position
            self.enterRemoveMode()
        
        return True
    
    def removeLocation(self):
    
        place = self.order.remove(self.currentItem)
        self.locations.remove(place)
        
        self.adapter.items.remove(place)
        self.listView.setAdapter(self.adapter)
        
        self.currentItem = -1
        self.enterPreviousMode()
    
    def cancelRemove(self):
    
        self.currentItem = -1
        self.enterPreviousMode()
    
    def enterPreviousMode(self):
    
        self.views.setDisplayedChild(self.view_indices[self.previous_mode])
        self.mode = self.previous_mode
    
    def enterMenuMode(self):
    
        self.views.setDisplayedChild(0)
        self.previous_mode = self.mode
        self.mode = "menu"
    
    def enterAddMode(self):
    
        self.views.setDisplayedChild(1)
        self.previous_mode = self.mode
        self.mode = "add"
    
    def enterRemoveMode(self):
    
        self.views.setDisplayedChild(2)
        self.previous_mode = self.mode
        self.mode = "remove"
    
    def startConfiguration(self):
        # This causes the LocationWidget to be hidden.
        self.configureHandler.startConfiguration()
    
    @args(FrameLayout.LayoutParams, [])
    def getViewParams(self):
    
        return FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER)
    
    @args(RelativeLayout.LayoutParams, [])
    def getAddParams(self):
    
        addParams = RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        addParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        addParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        addParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        return addParams


class MenuWidget(HBox):

    __interfaces__ = [View.OnClickListener]
    
    @args(void, [Context, ModeHandler])
    def __init__(self, context, handler):
    
        HBox.__init__(self, context)
        self.handler = handler
        
        self.addButton = Button(context)
        self.addButton.setText(R.string.add_location)
        self.addButton.setOnClickListener(self)
        
        self.configButton = Button(context)
        self.configButton.setText(R.string.configure_app)
        self.configButton.setOnClickListener(self)
        
        self.addWeightedView(self.addButton, 1)
        self.addWeightedView(self.configButton, 1)
    
    def onClick(self, view):
    
        if view == self.addButton:
            self.handler.enterAddMode()
        else:
            self.handler.startConfiguration()


class AddWidget(HBox):

    __interfaces__ = [View.OnClickListener, AdapterView.OnItemClickListener]
    
    @args(void, [Context, AddLocationListener, ModeHandler])
    def __init__(self, context, listener, handler):
    
        HBox.__init__(self, context)
        self.listener = listener
        self.handler = handler

        # Read the lists of place names and place specifications from the
        # application's resources, creating a dictionary from the two lists.
        resources = context.getResources()
        place_names = resources.getStringArray(R.array.place_names)
        
        self.places = dict(place_names,
                           resources.getStringArray(R.array.places))
        
        # Use a specialised adapter to provide filtered lists of data for an
        # auto-complete-enabled text view.
        self.adapter = FilterStringArrayAdapter(context,
            android.R.layout.simple_dropdown_item_1line, place_names)
        
        self.locationEdit = AutoCompleteTextView(context)
        self.locationEdit.setAdapter(self.adapter)
        self.locationEdit.setOnItemClickListener(self)
        
        self.cancelButton = ImageButton(context)
        self.cancelButton.setImageResource(android.R.drawable.ic_menu_close_clear_cancel)
        self.cancelButton.setOnClickListener(self)
        
        self.addWeightedView(self.locationEdit, 2)
        self.addWeightedView(self.cancelButton, 0)
    
    def onClick(self, view):
    
        if view == self.cancelButton:
            self.handler.enterMenuMode()
    
    def onItemClick(self, parent, view, position, id_):
    
        text = self.adapter.getItem(position)
        self.addLocation(text)
        self.locationEdit.clearFocus()
    
    @args(void, [String])
    def addLocation(self, text):
    
        name = text.trim()
        
        try:
            spec = self.places[name]
            
            # Remove the country from the name.
            name = name[:name.indexOf(", ")]
        
        except KeyError:
        
            # Replace spaces with underscores.
            spec = name.replace(" ", "_")
            
            # Split the name into pieces to check whether the user specified a
            # specifier directly.
            pieces = name.split("/")
            if len(pieces) < 3:
                return
            
            name = pieces[len(pieces) - 1]
            
            ### Potentially validate the specifier by requesting a page from
            ### yr.no.
        
        self.listener.addLocation(name, spec)
        self.locationEdit.setText("")


class RemoveWidget(HBox):

    __interfaces__ = [View.OnClickListener]
    
    @args(void, [Context, RemoveLocationListener])
    def __init__(self, context, handler):
    
        HBox.__init__(self, context)
        self.handler = handler
        
        self.removeButton = Button(context)
        self.removeButton.setText(R.string.remove_location)
        self.removeButton.setOnClickListener(self)
        
        self.cancelButton = Button(context)
        self.cancelButton.setText(R.string.cancel)
        self.cancelButton.setOnClickListener(self)
        
        self.addWeightedView(self.removeButton, 1)
        self.addWeightedView(self.cancelButton, 1)
    
    def onClick(self, view):
    
        if view == self.removeButton:
            self.handler.removeLocation()
        else:
            self.handler.cancelRemove()
